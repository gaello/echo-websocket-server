
// Print info about Node.JS version
console.log(new Date() + ' | Using Node ' + process.version + ' version to run server');


// Setting up server.
const http = require('http');
const server = http.createServer();
const port = 3000;

// Needed for parsing URLs.
const url = require('url');

// Setting WebSockets
const WebSocket = require('ws');
const wss = new WebSocket.Server({ noServer: true, clientTracking: true });

// Needed to generate player ids
const uuidv4 = require('uuid/v4');


// Websocket connection handler.
wss.on('connection', function connection(ws, request) {
  console.log(new Date() + ' | A new client is connected.');

  // Assign player Id to connected client.
  var uuidPlayer = uuidv4();

  // Registering player with the session.
  let sessionMsg = {};
  sessionMsg.type = "register";
  sessionMsg.method = "register";

  // Gathering player connection key.
  let playerKey = request.headers['sec-websocket-key'];
  sessionMsg.sessionId = playerKey;
  sessionMsg.uuidPlayer = uuidPlayer;

  // Sending confirm message to the connected client.
  ws.send(JSON.stringify(sessionMsg));

  // Handle all messages from users.
  ws.on('message', function(msgStr) {
    console.log('Message: '+msgStr);

    // Send back the same message.
    ws.send(msgStr);
  });

  // What to do when client disconnect?
  ws.on('close', function(connection) {
    console.log(new Date() + ' | Closing connection for a client.');

    // One of the clients has disconnected.
  });
});

// HTTP Server ==> WebSocket upgrade handling:
server.on('upgrade', function upgrade(request, socket, head) {

    console.log(new Date() + ' | Upgrading http connection to wss: url = '+request.url);

    // Parsing url from the request.
    var parsedUrl = url.parse(request.url, true, true);
    const pathname = parsedUrl.pathname

    console.log(new Date() + ' | Pathname = '+pathname);

    // If path is valid connect to the websocket.
    if (pathname === '/') {
      wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
      });
    } else {
      socket.destroy();
    }
});

// On establishing port listener.
server.listen(port, function() {
    console.log(new Date() + ' | Server is listening on port ' + port);

    // Server is running.
});
